using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DeactivateIfNotMine : MonoBehaviourPunCallbacks
{
      

    public Transform playerTransform;
    public float smoothSpeed = 0.125f; // Adjust this value to control camera smoothness

    private Vector3 offset;

    private void Start()
    {
        offset = transform.position - playerTransform.position;

        if (!photonView.IsMine)
        {
            gameObject.SetActive(false);
        }
    }

    private void LateUpdate()
    {
        Vector3 desiredPosition = playerTransform.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
        smoothedPosition.y = transform.position.y; // Keep the Y-position fixed

        transform.position = smoothedPosition;
    }
}
