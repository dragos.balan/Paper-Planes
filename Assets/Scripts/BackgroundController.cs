using Photon.Pun;
using UnityEngine;

public class BackgroundController : MonoBehaviourPun
{
    private Renderer backgroundRenderer;

    private void Awake()
    {
        backgroundRenderer = GetComponent<Renderer>();
    }

    private void Start()
    {
        // Check if the background object should be visible for this player
        if (!photonView.IsMine)
        {
            // Disable the renderer or set visibility to false for other players
            backgroundRenderer.enabled = false;
            // Or use backgroundRenderer.gameObject.SetActive(false);
        }
    }
}