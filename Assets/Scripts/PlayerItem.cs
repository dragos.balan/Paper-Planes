using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using PlayFab;
using PlayFab.ClientModels;

public class PlayerItem : MonoBehaviour
{
    public Text playerName;

    public Color highlightColor;

    public Image backgroundImage;



    public void Start()
    {
        backgroundImage = GetComponent<Image>();

  
    }

    public void SetPlayerInfo(Player player)
    {
        playerName.text = player.NickName;
    }

    public void ApplyLocalChanges()
    {
        if (backgroundImage != null)
        {
            backgroundImage.color = highlightColor;
        }
    }

    
}
