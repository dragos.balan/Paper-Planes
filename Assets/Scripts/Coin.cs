using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Coin : MonoBehaviourPunCallbacks
{
    public int value;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Plane") && photonView.IsMine)
        {
            Destroy(gameObject);
            CoinCounter.instance.IncreaseCoins(value);
            AudioManager.instance.PlaySFX("Coin");
        }
    }
}
