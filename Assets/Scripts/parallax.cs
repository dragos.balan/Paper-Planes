using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;



public class parallax : MonoBehaviour
{

    private float length, startpos;
    
    public float parallaxEffect;
    [Header("PlaneInCamera")]
    public GameObject player;
    public Camera cam;

    PhotonView view;

    // Start is called before the first frame update
    void Awake()
    {
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        // = GetComponent<PhotonView>();
        if(PhotonNetwork.LocalPlayer.ActorNumber == 1)
        {
            player = GameObject.FindGameObjectWithTag("Plane");
            cam = player.GetComponentInChildren<Camera>();
        }
       

    }

    // Update is called once per frame
    void Update()
    {
        //if(view.IsMine)
        //{
        if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
        {
            float temp = (cam.transform.position.x * (1 - parallaxEffect));
            float dist = (cam.transform.position.x * parallaxEffect);

            transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);

            if (temp > startpos + length) startpos += length;
            else if (temp < startpos - length) startpos -= length;

            //Debug.Log(PhotonNetwork.LocalPlayer.ActorNumber);
        }
        //}
        
    }
}
