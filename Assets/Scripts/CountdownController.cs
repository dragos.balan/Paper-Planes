using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;


public class CountdownController : MonoBehaviourPunCallbacks
{
    public static CountdownController instance;

    public int countdownTime;
    public Text countdownText;
    public GameObject CountdownPanel;

    public bool gamePlaying { get; private set; }

    private void Start()
    {
        gamePlaying = false;
        instance = this;

        StartCoroutine(CountdownToStart());
    }

    //public void StartCountdown()
    //{
    //    Debug.Log("im in!!!");
    //    StartCoroutine(CountdownToStart());
    //}

    private void BeginGame()
    {
        gamePlaying = true;
    }

    IEnumerator CountdownToStart()
    {
        while(countdownTime > 0)
        {
            countdownText.text = countdownTime.ToString();

            yield return new WaitForSeconds(1f);

            countdownTime--;
        }
        CountdownPanel.SetActive(false);
        BeginGame();

        countdownText.text = "GO!";

        //PhotonNetwork.LoadLevel("Main");

        yield return new WaitForSeconds(1f);

        countdownText.gameObject.SetActive(false);
        
    }
}
