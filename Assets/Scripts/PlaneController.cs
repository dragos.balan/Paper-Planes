using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections.Generic;

public class PlaneController : MonoBehaviourPunCallbacks
{
    public float flightSpeed = 3f; // Speed of flight
    public float forwardSpeed = 3f; // Speed of forward movement
    public float gravity = 3f; // Gravity force
    public float cameraFollowSpeed = 5f; // Speed of camera horizontal follow
    private bool isFlying = false; // Flag to determine if the plane is currently flying
    private Rigidbody2D rb; // Reference to the Rigidbody2D component of the plane
    [SerializeField] private Camera mainCamera; // Reference to the main camera
    public float maxDownRotation = -15f;
    public float maxUpRotation = 90f;
    public float maxSpeed = 20f;
    public float rotationSpeed = 100f;
    private LayerMask noRotationMask;
    private float initialYPosition;
    public Vector3 offset;
    public GameObject background;
    public SpriteRenderer Skin;
    public Text username;
    public GameObject WinnerScreen;
    public GameObject GameOverScreen;
    int trophy = 1;

    private static List<int> finishingOrder = new List<int>();
    private bool raceFinished = false;

    PhotonView view;

    //FinishLine finishLine;
    


    void Start()
    {
        //if (!view.IsMine) return;
        //WinnerScreen = GameObject.FindGameObjectWithTag("WinnerScreen");
        //WinnerScreen.SetActive(false);
        //GameOverScreen = GameObject.FindGameObjectWithTag("GameOver");
        //GameOverScreen.SetActive(false);

        rb = GetComponent<Rigidbody2D>();
    
        noRotationMask = 1 << LayerMask.NameToLayer("NoRotation");

        Application.targetFrameRate = 60;

        view = GetComponent<PhotonView>();

        Skin = GetComponent<SpriteRenderer>();

        foreach(ShopItem i in PlayfabManager.instance.ShopItems)
        {
            if(i.isEquipped && view.IsMine)
            {
                Skin.sprite = i.Sprite;
            }
        }

        
    }

    void Update()
    {
            if (view.IsMine)
            {
                if (CountdownController.instance.gamePlaying)
                {

                    rb.gravityScale = 1;
                    if (Input.touchCount > 0) // Check if there is a touch on the screen
                    {
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began) // If the touch has just begun
                    {
                        isFlying = true; // Set the flag to start flying
                    }
                    else if (touch.phase == TouchPhase.Ended) // If the touch has ended
                    {
                        isFlying = false; // Set the flag to stop flying
                    }
                }
            }
            }

            if(CountdownController.instance.gamePlaying)
            {
            Vector3 newPosition = mainCamera.transform.position;
            newPosition.y = offset.y;
            newPosition.z = offset.z;
            newPosition.x = gameObject.transform.position.x + offset.x;
            mainCamera.transform.position = newPosition;
            mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            
    }

    void FixedUpdate()
    {
        if(CountdownController.instance.gamePlaying)
        {
            if (isFlying) // If the plane is currently flying
            {
                rb.velocity = new Vector2(forwardSpeed, flightSpeed); // Set the velocity to fly upwards and move forward
            }
            else // If the plane is not flying
            {
                rb.velocity = new Vector2(forwardSpeed, -gravity); // Set the velocity to fall downwards and move forward
            }
            // Rotate the plane based on its velocity.
            float angle = Mathf.Atan2(rb.velocity.y - 4, rb.velocity.x + 2) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, angle), rotationSpeed * Time.deltaTime);
        }
         
        
    }

    //[SerializeField] private GameObject winnerScreen;

    //[PunRPC]
    //private void DeclareWinner(int winningPlayerID)
    //{
        
    //    // Compare the winning player's ID with the local player's ID
    //    if (PhotonNetwork.LocalPlayer.ActorNumber == winningPlayerID)
    //    {
    //        // Perform actions for the local player winning the race
    //        // For example, display a victory screen or initiate end-of-game logic
    //        WinnerScreen.SetActive(true);
    //        PlayfabManager.instance.SendLeaderboard(trophy);
    //        GameOverScreen.SetActive(false);
    //    }
        
    //}
}
   

