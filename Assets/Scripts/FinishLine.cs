using UnityEngine;
using UnityEngine.SceneManagement;
using PlayFab;
using PlayFab.ClientModels;
using Photon.Pun;
using Unity.VisualScripting;

public class FinishLine : MonoBehaviourPun
{
    
    public GameObject GameOverScreen;
    public GameObject settingsPanel;
    public GameObject mainMenuPanel;
    public GameObject WinnerScreen;

    //private bool raceFinished = false;
    int trophy = 1;


    //private void OnTriggerEnter2D(Collider2D other)
    //{
    //    // Check if the other collider is a plane.
    //    //PlaneController plane = other.GetComponent<PlaneController>();
    //    if (other.CompareTag("Plane"))
    //    {
    //        //if (!raceFinished)
    //        //{
    //        //    // The player has crossed the finish line
    //        //    raceFinished = true;

    //        //    // Inform other players that this player has finished
    //        //    PhotonView photonView = other.GetComponent<PhotonView>();
    //        //    photonView.RPC("PlayerFinished", RpcTarget.AllBuffered);

    //        //    PlayfabManager.instance.SendLeaderboard(trophy);
    //        //}

    //        PhotonView photonView = other.GetComponent<PhotonView>();

    //        if(photonView.IsMine)
    //        {
    //            photonView.RPC("DeclareWinner", RpcTarget.All, photonView.OwnerActorNr);
    //            Debug.Log("IN RPC!!!");
    //            //Time.timeScale = 0;
    //        }


    //        // Stop the game.
    //        Time.timeScale = 0;

    //        GameOverScreen.SetActive(true);


    //        Debug.Log("Game Over!");

    //        GrantVirtualCurrency();
    //        //PlayfabManager.instance.SendLeaderboard(trophy);

    //    }
    //}

    private bool raceFinished = false;
    private PhotonView photonView;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!raceFinished && collision.CompareTag("Plane"))
        {
            raceFinished = true;
            string winningPlayerID = collision.gameObject.GetComponent<PhotonView>().Owner.UserId;
            photonView.RPC("FinishRace", RpcTarget.All, winningPlayerID);
        }
    }

        public void MainMenuButton()
    {
        SceneManager.LoadScene("MenuScene");
        Time.timeScale = 1;
        PhotonNetwork.Disconnect();
    }

    public void GrantVirtualCurrency()
    {
        var request = new AddUserVirtualCurrencyRequest
        {
            VirtualCurrency = "CN",
            Amount = CoinCounter.instance.currentCoins
        };
        PlayFabClientAPI.AddUserVirtualCurrency(request, OnGrantVirtualCurrencySuccess, OnError);
    }

    void OnGrantVirtualCurrencySuccess(ModifyUserVirtualCurrencyResult result)
    {
        Debug.Log("Currency granted!");
    }

    void OnError(PlayFabError error)
    {
        Debug.Log("Error while granting currency!");
    }

    [PunRPC]
    private void FinishRace(string winningPlayerID)
    {
        string localPlayerID = PhotonNetwork.LocalPlayer.UserId;

        if (winningPlayerID == localPlayerID)
        {
            Debug.Log("You Win!");
            // Activate the win canvas for the local player
            WinnerScreen.gameObject.SetActive(true);
            PlayfabManager.instance.SendLeaderboard(trophy);
        }
        else
        {
            Debug.Log("You Lost!");
            // Activate the lose canvas for other players
            GameOverScreen.gameObject.SetActive(true);
        }

        // Stop the game for all players
        Time.timeScale = 0f;
    }

}
