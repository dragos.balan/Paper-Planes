using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using PlayFab;
using PlayFab.ClientModels;

public class ConnectToServer : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest(), OnGetPlayerProfile, OnGetPlayerProfileFailure);
        PhotonNetwork.ConnectUsingSettings();
        
    }

    private void OnGetPlayerProfile(GetPlayerProfileResult result)
    {
        // Retrieve the display name from the result and assign it to the text element
        PhotonNetwork.NickName = result.PlayerProfile.DisplayName;
    }

    private void OnGetPlayerProfileFailure(PlayFabError error)
    {
        Debug.LogError("Failed to retrieve player profile: " + error.ErrorMessage);
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        SceneManager.LoadScene("Lobby");
    }
}
