using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro.Examples;

public class ParallaxBackground : MonoBehaviourPunCallbacks
{
    public float parallaxEffect;
    public GameObject player;

    private Camera cam;
    private Transform camTransform;
    private Vector3 previousCamPosition;

    private void Start()
    {
       
        if (photonView.IsMine)
        {
            cam = Camera.main;
            camTransform = cam.transform;
            previousCamPosition = camTransform.position;

        }
    }

    private void Update()
    {

        if (player != null && cam != null)
        {
            float parallaxOffset = (previousCamPosition.x - camTransform.position.x) * parallaxEffect;
            transform.position += new Vector3(parallaxOffset, 0f, 0f);

            previousCamPosition = camTransform.position;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(transform.position);
        }
        else
        {
            transform.position = (Vector3)stream.ReceiveNext();
        }
    }
}
