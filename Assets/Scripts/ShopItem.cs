using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using System.Linq;

[System.Serializable]
public class ShopItem : MonoBehaviour
{
    public static ShopItem instance { get; set; }

    public int Cost;
    public string Name;
    public Sprite Sprite;
    public Button BuyButton;
    public Button EquipButton;
    public bool isEquipped;

    private void Start()
    {
        instance = this;
        if(isEquipped)
        {
            EquipButton.interactable = false;
        }else
        {
            EquipButton.interactable = true;
        }
      
    }

    public void clickButtonEquip()
    {
        foreach(ShopItem i in PlayfabManager.instance.ShopItems)
        {
            if(i.Name != this.Name)
            {
                i.isEquipped = false;
                i.EquipButton.interactable = true;
            }
            else
            {
                i.isEquipped = true;
                i.EquipButton.interactable = false;
            }
        }
    }


}