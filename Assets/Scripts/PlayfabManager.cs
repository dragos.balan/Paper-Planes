using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using System.Linq;


public class PlayfabManager : MonoBehaviour
{
    public static PlayfabManager instance;

    public Text coinsValueText;
    public Text shopCoinsText;

    public ShopItem[] ShopItems;
    public ShopItem shopItem;

    public MenuController menuController;

    public GameObject contentArea;
    public GameObject buttonObj;

    public string itemToCheck;

    public GameObject rowPrefab;
    public Transform rowsParent;

    public GameObject UsernamePanel;
    public GameObject mainMenuPanel;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Login();

    }

    void Login()
    {
        var request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true
            }
            
        };
        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);



    }

    void OnSuccess(LoginResult result)
    {
        Debug.Log("Successful login/account create!");
        GetVirtualCurrency();
        GetItemPrices();

        string name = null;

        if(result.InfoResultPayload.PlayerProfile != null)
        {
            name = result.InfoResultPayload.PlayerProfile.DisplayName;

        }

        if(name == null)
        {
            UsernamePanel.SetActive(true);
            mainMenuPanel.SetActive(false);
            
        }
    }

    void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging in/creating account!");
        Debug.Log(error.GenerateErrorReport());
    }


    //CURRENCY
    public void GetVirtualCurrency()
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), OnGetUserInventorySuccess, OnErrorCoins);
    }

    void OnGetUserInventorySuccess(GetUserInventoryResult result)
    {
        int coins = result.VirtualCurrency["CN"];
        coinsValueText.text = " " + coins.ToString();
        shopCoinsText.text = " " + coins.ToString();

    }

    void OnErrorCoins(PlayFabError error)
    {
        Debug.Log("Error while getting Coins!");
    }

    //SHOP
    public void GetItemPrices()
    {
        GetCatalogItemsRequest request = new GetCatalogItemsRequest();
        request.CatalogVersion = "Skins";
        PlayFabClientAPI.GetCatalogItems(request, result =>
        {
            List<CatalogItem> items = result.Catalog;
            foreach (CatalogItem i in items)
            {
                uint cost = i.VirtualCurrencyPrices["CN"];
                foreach (ShopItem editorItems in ShopItems)
                {

                    if (editorItems.Name == i.ItemId)
                    {
                        editorItems.Cost = (int)cost;
                        Debug.Log(editorItems.Cost);
                    }

                }
                Debug.Log(cost);
            }

            foreach (ShopItem i in ShopItems)
            {
                
                i.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = i.Cost.ToString();

                VerifyItemInInventory(i.Name);

                i.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate
                {
                    MakePurchase(i.Name, i.Cost);

                });
                VerifyItemInInventory(i.Name);
            }
        }, error =>
        {

        });
    }

    void MakePurchase(string name, int price)
    {
        PurchaseItemRequest request = new PurchaseItemRequest();
        request.CatalogVersion = "Skins";
        request.ItemId = name;
        request.VirtualCurrency = "CN";
        request.Price = price;

        PlayFabClientAPI.PurchaseItem(request, result =>
        {
            GetVirtualCurrency();
            VerifyItemInInventory(name);

        }, error =>
        {
            Debug.Log(error.ErrorMessage);
        });


    }


    private void VerifyItemInInventory(string itemId)
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), (result) =>
        {
            // Check if the item exists in the player's inventory
            bool itemExists = result.Inventory.Exists(item => item.ItemId == itemId);

            if (itemExists)
            {
                // The player has the item in their inventory
                Debug.Log("Item is in the player's inventory: " + itemId);
                foreach (ShopItem i in ShopItems)
                {
                    if(i.Name == itemId)
                    {
                        i.transform.GetChild(1).gameObject.SetActive(false);
                        i.transform.GetChild(2).gameObject.SetActive(true);
                    }
                    
                }

            }
            else
            {
                // The player does not have the item in their inventory
                Debug.Log("Item is NOT in the player's inventory: " + itemId);
            }
        }, OnGetUserInventoryFailure);
    }

    private void OnGetUserInventoryFailure(PlayFabError error)
    {
        Debug.Log("Failed to get user inventory");
        Debug.Log(error.GenerateErrorReport());
    }


    //LEADERBOARD
    public void SendLeaderboard(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "Trophies",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderboardUpdate, OnError);
    }

    void OnLeaderboardUpdate(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Successfull leaderboard sent");
    }

    public void GetLeaderboard()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "Trophies",
            StartPosition = 0,
            MaxResultsCount = 10
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, OnError);
    }

    void OnLeaderboardGet(GetLeaderboardResult result)
    {
        foreach (Transform item in rowsParent)
        {
            Destroy(item.gameObject);
        }

        foreach (var item in result.Leaderboard)
        {
            GameObject newGo = Instantiate(rowPrefab, rowsParent);
            Text[] texts = newGo.GetComponentsInChildren<Text>();
            texts[0].text = item.DisplayName;
            texts[1].text = (item.Position + 1).ToString();
            texts[2].text = item.StatValue.ToString();

            Debug.Log(item.Position + " " + item.PlayFabId + " " + item.StatValue);
        }

    }

   
}



    