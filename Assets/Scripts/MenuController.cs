using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;


public class MenuController : MonoBehaviour
{
    public GameObject settingsPanel;
    public GameObject mainMenuPanel;
    public GameObject shopPanel;
    public GameObject TrophyPanel;
    public GameObject UsernamePanel;
    public Text coinsText;

    public InputField nameInput;


    private void Start()
    {
        settingsPanel = GameObject.FindGameObjectWithTag("Settings");
        mainMenuPanel = GameObject.FindGameObjectWithTag("MainMenu");
        shopPanel = GameObject.FindGameObjectWithTag("shop");
        TrophyPanel = GameObject.FindGameObjectWithTag("TrophyPanel");
        UsernamePanel = GameObject.FindGameObjectWithTag("UsernamePanel");

        settingsPanel.SetActive(false);
        mainMenuPanel.SetActive(true);
        shopPanel.SetActive(false);
        TrophyPanel.SetActive(false);
        UsernamePanel.SetActive(false);


        AudioManager.instance.musicSource.Play();
    }


    public void PlayButton()
    {
        SceneManager.LoadScene("LoadingRandomPlay");
    }

    public void SettingsButton()
    {
        settingsPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void backButton()
    {
        settingsPanel.SetActive(false);
        mainMenuPanel.SetActive(true);
        shopPanel.SetActive(false);
        TrophyPanel.SetActive(false);
    }

    public void PartyButton()
    {
        SceneManager.LoadScene("Loading");
    }

    public void shopButton()
    {
        mainMenuPanel.SetActive(false);
        shopPanel.SetActive(true);
    }

    public void TrophyButton()
    {
        mainMenuPanel.SetActive(false);
        TrophyPanel.SetActive(true);

        PlayfabManager.instance.GetLeaderboard();
    }

    public void SubmitNameButton()
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = nameInput.text,
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnError);
        UsernamePanel.SetActive(false);
        mainMenuPanel.SetActive(true);
    }

    void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Updated Display Name!");
    }

    void OnError(PlayFabError error)
    {
        Debug.Log("Error while submitting username !");
        Debug.Log(error.GenerateErrorReport());
    }
   
}
