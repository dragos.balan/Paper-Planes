using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Slider _Slider;

    public void MasterVolume()
    {
        AudioManager.instance.MasterVolume(_Slider.value);
        
    }
}
